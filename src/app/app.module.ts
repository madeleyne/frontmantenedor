import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdmminMantenedorModule } from './admin-mantenedor/admmin-mantenedor.module';
import { SesionComponent } from './admin-mantenedor/sesion/sesion.component';
import { AdminActividadesComponent } from './admin-mantenedor/admin-actividades/admin-actividades.component';
import { AdminActividadesModule } from './admin-actividades/admin-actividades.module';
import { AdminUsuariosModule } from './admin-usuarios/admin-usuarios.module';
import { AgregarActividadComponent } from './admin-actividades/agregar-actividad/agregar-actividad.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ActividadService } from './shared/actividad.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { ToastrModule } from 'ngx-toastr';
import { Ng2Rut} from 'ng2-rut';
import { EliminarRegistroComponent } from './admin-usuarios/eliminar-registro/eliminar-registro.component';


@NgModule({
  declarations: [
    AppComponent,
    SesionComponent,
    AdminActividadesComponent,
    AgregarActividadComponent,
    EliminarRegistroComponent        
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AdmminMantenedorModule,
    AdminActividadesModule,
    AdminUsuariosModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2Rut,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      timeOut: 3000
    })
    
    
  ],
  providers: [ActividadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
