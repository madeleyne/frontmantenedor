export class RegistrosResponse {

    Rut: string;
    Nombre: string;
    ApPaterno: string;
    ApMaterno: string;
    Telefono: string;
    Correo: string;
    Password: string;
    ImagenPerfil: string;
}