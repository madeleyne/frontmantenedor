import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActividadService } from 'src/app/shared/actividad.service';
import { Actividad } from './Models/actividad.model';
import { ToastrService } from 'ngx-toastr';
import { ActividadesResponse } from './Models/actividadesResponse';
import { EditarResponse } from './Models/editarResponse';

@Component({
  selector: 'app-agregar-actividad',
  templateUrl: './agregar-actividad.component.html',
  styleUrls: ['./agregar-actividad.component.css']
})
export class AgregarActividadComponent implements OnInit {

  private idActual: number;

  //ruta donde se encuentran las imagenes
  private urlImagenes: string = 'https://localhost:5001/api/image/';

  //variable para crear un nuevo formulario con los input del formulario
  private formData: Actividad;
  private formdataEditar: EditarResponse;

  //variables para la previsualizacion de la imagen

  FileName: string = "Seleccionar Imagen";
  FileToUpload: File = null;
  ImagenURL: string = "assets/img/imagenNoDisponible.png";

  //variables para previsualizacion del editar
  FileNameEditar: string;
  FileToUploadEditar: File = null;
  ImagenURLeditar: string;


  //variables para el control del formulario
  public crearActividadForm: FormGroup;
  public nombreActividad: FormControl;
  public descripActividad: FormControl;
  public imagenActividad: FormControl;

  //variables para la lista de actividades
  public actividadResponse: ActividadesResponse[];

  //varibles para el formulario para editar
  public modificarActividadForm: FormGroup;
  public editarNombreActividad: FormControl;
  public editarDescripActividad: FormControl;
  public editarImagenActividad: FormControl;



  constructor(private router: Router, private service: ActividadService, private toastr: ToastrService) {

    //validadores del formulario
    this.nombreActividad = new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-zñÑ ]{2,20}$/)]);
    this.descripActividad = new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-zñÑ ]{2,60}$/)]);
    this.imagenActividad = new FormControl('', Validators.required);

    this.actividadResponse = [];

    //validadores formulario de edicion
    this.editarNombreActividad = new FormControl('', [Validators.pattern(/^[A-Za-zñÑ ]{2,20}$/)]);
    this.editarDescripActividad = new FormControl('', [Validators.pattern(/^[A-Za-zñÑ ]{2,20}$/)]);
    this.editarImagenActividad = new FormControl('');

  }

  ngOnInit() {
    this.crearActividadForm = new FormGroup({
      nombreActividad: this.nombreActividad,
      descripActividad: this.descripActividad,
      imagenActividad: this.imagenActividad
    });

    this.traerActividad();

    this.modificarActividadForm = new FormGroup({
      editarNombreActividad: this.editarNombreActividad,
      editarDescripActividad: this.editarDescripActividad,
      editarImagenActividad: this.editarImagenActividad
    });
  }

  //metodo para la previsualizacion de la imagen al seleccionarla
  UpdateControls(e): void {
    this.FileToUpload = e.target.files.item(0);
    this.FileName = this.FileToUpload.name;

    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.ImagenURL = event.target.result;
    }

    reader.readAsDataURL(this.FileToUpload);

  }

  //metodo para resetear el formulario
  resetFormulario() {

    this.nombreActividad.setValue('');
    this.descripActividad.setValue('');
    this.imagenActividad.setValue('');
    this.FileToUpload = null;
    this.FileName = 'Seleccionar Imagen';
    this.ImagenURL = "assets/img/imagenNoDisponible.png";
  }

  //metodo para hacer el post utilizando el servicio actividad.service
  creacionDeActividades() {

    if (this.crearActividadForm.valid) {

      this.formData = new Actividad();

      this.formData.nombreActividad = this.nombreActividad.value;
      this.formData.descripActividad = this.descripActividad.value;
      this.formData.imagenActividad = this.FileToUpload.name;

      this.service.postActividad(this.formData, this.FileToUpload).subscribe(res => {

        this.toastr.success("Actividad agregada exitosamente");
        this.traerActividad();
        this.resetFormulario();


      },
        err => {
          this.toastr.error(err.error, "ERROR");
          this.resetFormulario();
        }
      );


    } else {
      if (this.nombreActividad.untouched) {
        this.toastr.error("Debe ingresar un nombre", "ATENCIÓN");
      } if (this.descripActividad.untouched) {
        this.toastr.error("Debe ingresar una descripción", "ATENCIÓN");

      } if (this.imagenActividad.untouched) {
        this.toastr.error("Debe agregar una imagen", "ATENCIÓN");

      }


    }

  }

  //método para traer las actividades del servicio
  traerActividad() {

    this.service.getActividad().subscribe(res => {

      this.actividadResponse = res;
    })

  }

  //método para traer la actividad por id y así modificarla

  traerActividadPorId(id: any) {

    this.service.getActividadPorId(id).subscribe(res => {

      this.modificarActividadForm.controls.editarNombreActividad.setValue(res.Nombreactividad);
      this.modificarActividadForm.controls.editarDescripActividad.setValue(res.Descripactividad);
      this.ImagenURLeditar = this.urlImagenes + res.Imagenactividad;
      this.idActual = res.Idactividades;
      //console.log(this.idActual);

    })
  }


  //método para eliminar las actividades por id
  eliminarActividad(id: any) {

    if (confirm("Esta seguro de eliminar esta actividad?")) {

      this.service.deleteActividad(id).subscribe(res => {

        this.traerActividad();
        this.toastr.warning("Actividad eliminada correctamente");

      },
        err => {
          this.toastr.error(err.error, "Atención");


        })
    }
  }


  updateImagenAEditar(evento): void {

    //previsualizacion del nombre de la imagen
    this.FileToUploadEditar = evento.target.files.item(0);
    this.FileNameEditar = this.FileToUploadEditar.name;

    //previsualizacion de la nueva imagen
    var nuevaImagen = new FileReader();

    nuevaImagen.onload = (event: any) => {

      this.ImagenURLeditar = event.target.result;
    }

    nuevaImagen.readAsDataURL(this.FileToUploadEditar);

  }

  //método para editar las actividades
  modificarActividad() {


    if (this.modificarActividadForm.valid) {

      this.formdataEditar = new EditarResponse();

      this.formdataEditar.nombreActividad = this.editarNombreActividad.value;
      this.formdataEditar.descripActividad = this.editarDescripActividad.value;
      this.formdataEditar.imagenActividad = this.FileToUploadEditar.name;


      this.service.putActividad(this.formdataEditar, this.FileToUploadEditar).subscribe(res => {

        this.toastr.success("Actividad modificada exitosamente");
        this.service.deleteActividad(this.idActual).subscribe(res => {
          this.traerActividad();          
        })



      },
        err => {
          this.toastr.error(err.error, "ERROR");
          this.resetFormulario();
        });


    } 
    else {
      if (this.editarNombreActividad.untouched) {
        this.toastr.error("Debe ingresar un nombre", "ATENCIÓN");
      } if (this.editarDescripActividad.untouched) {
        this.toastr.error("Debe ingresar una descripción", "ATENCIÓN");
      } if (this.editarImagenActividad.untouched) {
        this.toastr.error("Debe agregar una imagen", "ATENCIÓN");

      }
    }


  }



}