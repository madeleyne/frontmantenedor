import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SesionComponent } from './admin-mantenedor/sesion/sesion.component';
import { AdminActividadesComponent } from './admin-mantenedor/admin-actividades/admin-actividades.component';
import { AgregarActividadComponent } from './admin-actividades/agregar-actividad/agregar-actividad.component';
import { EliminarRegistroComponent } from './admin-usuarios/eliminar-registro/eliminar-registro.component';

const routes: Routes = [
  {path: 'home', component:SesionComponent},
  {path: 'AdministrarActividades', component:AdminActividadesComponent},
  {path: 'AgregarActividad', component: AgregarActividadComponent},
  {path: 'EliminarRegistro', component: EliminarRegistroComponent},
  {path: '**', component:SesionComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
