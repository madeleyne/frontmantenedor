import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';




@Component({
  selector: 'app-sesion',
  templateUrl: './sesion.component.html',
  styleUrls: ['./sesion.component.css']
})
export class SesionComponent implements OnInit {


  //variables para hacer el control de los input del formulario
  public inicioSesionForm: FormGroup;
  public rutAdmin: FormControl;
  public password: FormControl;


  constructor(private router: Router, private toastr: ToastrService) {

    //agrego al constructor las variables que voy a controlar
    this.rutAdmin = new FormControl('', [Validators.required,Validators.pattern(/^[0-9\.K-k\-]{9,12}$/)]);
    this.password = new FormControl('', Validators.required);

  }

  ngOnInit() {

    this.inicioSesionForm = new FormGroup({
      rutAdmin: this.rutAdmin,
      password: this.password
    })



  }

  //metodo para redireccionar a la página administrar actividades
  ingresoSesion() {   
    
    this.router.navigate(['AdministrarActividades']);

    /*
    if (this.inicioSesionForm.valid) {

      let rutUsuarioTemp = "12.345.678-9";
      let passUsuarioTemp = "123456";

      if (rutUsuarioTemp == this.rutAdmin.value && passUsuarioTemp == this.password.value) {
        this.router.navigate(['AdministrarActividades']);
      } else {
        console.log(this.rutAdmin.value);
        console.log(this.password.value);
        this.toastr.warning("Usuario y/o contraseña no válido");
        //this.resetFormulario();
      }

    } else {
      if (this.rutAdmin.untouched) {
        this.toastr.info("Debe ingresar un RUT");
      }

      if (this.password.untouched) {
        this.toastr.info("Debe ingresar una contraseña");
      }
    } */
  }

  resetFormulario() {
    this.rutAdmin.setValue('');
    this.password.setValue('');
  }


}
