import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-actividades',
  templateUrl: './admin-actividades.component.html',
  styleUrls: ['./admin-actividades.component.css']
})
export class AdminActividadesComponent implements OnInit {



  constructor(private router: Router) { }

  ngOnInit() {


  }

  //Redireccionamiento Actividades
  agregarAct(){

    this.router.navigate(['AgregarActividad']); 
    
    //document.getElementById("llamarCollapse").style.display = "inline";
    //document.getElementById("dropdownMenuButton").style.display = "none";

  }

  modificarAct(){
    
    this.router.navigate(['AgregarActividad']);
    //document.getElementById("llamarCollapse").style.display = "none";
    //document.getElementById("dropdownMenuButton").style.display = "inline";

  }

  //Redireccionammiento Registros
  eliminarRegistrosUsuarios(){
    this.router.navigate(['EliminarRegistro']);

  }




  //Cerrar Sesion
  cerrarSesion(){
    this.router.navigate(['Home']);

  }

  

  

}
