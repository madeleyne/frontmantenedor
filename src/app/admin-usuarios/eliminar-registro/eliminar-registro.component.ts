import { Component, OnInit } from '@angular/core';
import { RegistrosResponse } from 'src/app/admin-actividades/agregar-actividad/Models/registrosResponse';
import { RegistroService } from 'src/app/shared/registro.service';

@Component({
  selector: 'app-eliminar-registro',
  templateUrl: './eliminar-registro.component.html',
  styleUrls: ['./eliminar-registro.component.css']
})
export class EliminarRegistroComponent implements OnInit {

  //variable para lista de registros
  public registrosResponse: RegistrosResponse[];

  constructor(private serviceRegistro: RegistroService) { 

    
  }

  ngOnInit() {

    this.traerRegistros();
  }

  traerRegistros(){

    this.serviceRegistro.getRegistro().subscribe(res => {

        this.registrosResponse = res;
    })

  }

}
