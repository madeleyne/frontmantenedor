import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RegistrosResponse } from '../admin-actividades/agregar-actividad/Models/registrosResponse';

@Injectable({
    providedIn: 'root'
})

export class RegistroService {

    //ruta de acceso
    readonly rootURL = "https://localhost:5001/api/";
    private headers: HttpHeaders;

    constructor(private httpClient: HttpClient) {
        this.headers = new HttpHeaders();
    }

    //método para realizar el get
    getRegistro(): Observable<RegistrosResponse[]> {

        return this.httpClient.get<RegistrosResponse[]>(this.rootURL + "registroes");
    }

    eliminarUsuario(id: number){
        
        return this.httpClient.delete(this.rootURL + "registroes/" + id);

    }
}