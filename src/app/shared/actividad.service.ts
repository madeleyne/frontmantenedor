import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Actividad } from '../admin-actividades/agregar-actividad/Models/actividad.model';
import { Observable } from 'rxjs';
import { ActividadesResponse } from '../admin-actividades/agregar-actividad/Models/actividadesResponse';
import { EditarActividad } from '../admin-actividades/agregar-actividad/Models/editarActividad';
import { EditarResponse } from '../admin-actividades/agregar-actividad/Models/editarResponse';

@Injectable({
  providedIn: 'root'
})
export class ActividadService {

  //ruta de acceso
  readonly rootURL = "https://localhost:5001/api/";
  private headers: HttpHeaders;


  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
  }


  //metodo con los parametros necesarios para realizar el post
  postActividad(actividadModel: Actividad, fileToUpload: File) {

    //variable temporal para crear un nuevo formulario con los parametros necesarios
    let formToPost = new FormData();

    //variable temporal para pasar todas mis variables a tipo string
    let requestToPost = JSON.stringify({

      nombreActividad: actividadModel.nombreActividad,
      descripActividad: actividadModel.descripActividad,
      imagenActividad: actividadModel.imagenActividad
    });

    //agrego al nuevo formulario los parametros que necesita para hacer el post
    formToPost.append("actividades", requestToPost);
    formToPost.append("imagenActividad", fileToUpload, fileToUpload.name);

    //envio los parametros y la URL a traves de un http
    return this.httpClient.post(this.rootURL + "actividades", formToPost);
  }


  //método para realizar el get
  getActividad(): Observable<ActividadesResponse[]> {

    return this.httpClient.get<ActividadesResponse[]>(this.rootURL + "actividades");
  }


  //método para eliminar actividades

  deleteActividad(id: number) {

    return this.httpClient.delete(this.rootURL + "actividades/" + id);

  }

  //método para traer las actividades por id
  getActividadPorId(id: number): Observable<ActividadesResponse> {

    return this.httpClient.get<ActividadesResponse>(this.rootURL + "actividades/" + id);

  }

  //método para editar las actividades
  putActividad(editarActividad: EditarResponse, fileToUpload: File) {

    //variable temporal para crear un nuevo formulario con los parametros necesarios
    let formToPut = new FormData();

    //variable temporal para pasar todas mis variables a tipo string
    let requestToPut = JSON.stringify({

      nombreActividad: editarActividad.nombreActividad,
      descripActividad: editarActividad.descripActividad,
      imagenActividad: editarActividad.imagenActividad
    });

    //agrego al nuevo formulario los parametros que necesita para hacer el post
    formToPut.append("actividades", requestToPut);
    formToPut.append("imagenActividad", fileToUpload, fileToUpload.name);

    //envio los parametros y la URL a traves de un http
    return this.httpClient.put<EditarResponse>(this.rootURL + "actividades", formToPut);

  }




}
